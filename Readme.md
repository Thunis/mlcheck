*mlcheck* creates a report about an email address.
It lists whether it is listed in a member list and also checks for its membership in various ezmlm lists.
A report looks like this:

<pre>
$ mlcheck bob@example.com
<b>bob@example.com</b>
ist <span style="color:green"><b>Vereinsmitglied</b></span>
Abonnementlevel: <b>2</b>
Projekte: zusammengesetzt, oederland
</pre>
