#!/usr/bin/env python3

import subprocess
from shlex import quote
from sys import argv,stderr,exit
import os
import json

if len(argv) != 2:
    print("Usage: {} emailaddress".format(argv[0]),file=stderr)
    exit(1)

address = argv[1]

with open(os.path.dirname(os.path.realpath(__file__))+"/config.json","r") as f:
    config = json.loads(''.join(f.readlines()))

def bold(s):
    return "\033[1m" + s + "\033[0m"
def red(s):
    return "\033[1;31m" + s + "\033[0m"
def green(s):
    return "\033[1;32m" + s + "\033[0m"

def file_contains(f,s):
    v = subprocess.call(["grep","--fixed-strings","--quiet","--ignore-case",s,f])
    return v == 0

def ml_contains(ml,adr):
    v = subprocess.call("ezmlm-list {} | grep --fixed-strings --quiet --ignore-case {}".format(quote(ml),quote(adr)), shell=True)
    return v == 0

def file_list(path):
    return sorted(os.listdir(path))

def get_level(is_announce, is_public, is_orga):
    t = (is_announce, is_public, is_orga)
    if t == (False,False,False):
        return 0
    elif t == (True,False,False):
        return 1
    elif t == (True,True,False):
        return 2
    elif t == (True,True,True):
        return 3
    else:
        return None

def levelstr(level):
    if level == None:
        return red("Mitgliedschaften sind inkonsistent mit den Leveln")
    else:
        return bold(str(level))

is_member = file_contains(config["member_csv_file"], address)
is_announce, is_public, is_orga = map(lambda x: ml_contains(config["main_mailinglists_directory"]+"/"+x, address), ["announce","public","orga"])
level = get_level(is_announce, is_public, is_orga)
print(bold(address))
print("ist "+green("Vereinsmitglied") if is_member else "ist "+red("kein Vereinsmitglied"))
print("Abonnementlevel: "+levelstr(level))
if level == None:
    print("""
        Announce: {}
        Public: {}
        Orga: {}
        """.format(is_announce, is_public, is_orga))
allprojects = file_list(config["projects_ml_dir"])
projects = list(filter(lambda x: ml_contains(config["projects_ml_dir"]+"/"+x, address), allprojects))
print("Projekte: "+(", ".join(projects)))
